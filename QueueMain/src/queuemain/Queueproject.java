/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queuemain;
import java.util.*;

/**
 *
 * @author Administrator
 */
public class Queueproject {
    private int arr[];
    private int capacity;
    private int front;
    private int rear;
    private int count;
    
    Queueproject(int size)
    {
        arr=new int[size];
        capacity=size;
        front=0;
        rear=-1;
        count=0;
    }
    public int size()
    {
        return count;
    }
    public boolean isEmpty()
    {
      return (size()==0);
      
    }
    public boolean isFull()
    {
        return (size()==capacity);
    }
    public int peek()
	{
		if (isEmpty()) 
		{
			System.out.println("UnderFlow\nProgram Terminated");
			System.exit(1);
		}
		return arr[front];
	}
    public void enqueue(int item)
	{
		// check for queue overflow
		if (isFull())
		{
			System.out.println("OverFlow\nProgram Terminated");
			System.exit(1);
		}

		System.out.println("Inserting " + item);

		rear = rear + 1;
		arr[rear] = item;
		count++;
	}
    public void dequeue()
	{
		// check for queue underflow
		if (isEmpty())
		{
			System.out.println("UnderFlow\nProgram Terminated");
			System.exit(1);
		}

		System.out.println("Removing " + arr[front]);

		front = front + 1;
		count--;
	}
}
